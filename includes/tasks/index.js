import TaskHtml from './TaskHtml';
import TaskSass from './TaskSass';
import TaskJs   from './TaskJs';
import TaskImgStatic   from './TaskImgStatic';
import TaskImgSprite   from './TaskImgSprite';
import TaskSvgSprite   from './TaskSvgSprite';
import TaskCopy   from './TaskCopy';
// import TaskPicMin   from './TaskPicMin';
// import TaskStaticMin   from './TaskStaticMin';

export {TaskHtml, TaskSass, TaskJs, TaskImgStatic, TaskImgSprite, TaskSvgSprite, TaskCopy};